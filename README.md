# landing-page_mvc
<h2>Sobre o Projeto</h2>
<p>Essa landing page possui um layout atraente com fins de apresentação, e foi criada focando mais no sistema de rotas.</p>
<h3>Front-end:</h3>
<ul>
  <li>HTML</li>
  <li>CSS</li>
  <li>JS</li>
</ul>
<h3>Back-end:</h3>
<ul>
  <li>PHP</li>
  <li>Database: MySql</li>
</ul>

![gif (2)](https://user-images.githubusercontent.com/82960240/138711298-3e381400-6ed7-43b3-87aa-ba838a475745.gif)

<h3>Autor</h3>
<h4>Raissa Arcaro Daros</h4>
<div style="display: inline_block;"><br>
   
[![Blog](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/raissa_dev/)
[![Blog](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/raissa-dev-69986a214/)
[![Blog](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/Raissadev/)  
   
</div>
